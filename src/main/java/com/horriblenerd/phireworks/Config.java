package com.horriblenerd.phireworks;

import net.minecraftforge.common.ForgeConfigSpec;

/**
 * Created by HorribleNerd on 29/12/2020
 */
public class Config {

    public static final String CATEGORY_GENERAL = "general";

    public static final ForgeConfigSpec COMMON_CONFIG;

    public static final ForgeConfigSpec.BooleanValue ALLOW_FLICKER;
    public static final ForgeConfigSpec.IntValue COLOR_COUNT;
    public static final ForgeConfigSpec.IntValue MIN_HEIGHT;
    public static final ForgeConfigSpec.IntValue MAX_HEIGHT;

    static {
        ForgeConfigSpec.Builder COMMON_BUILDER = new ForgeConfigSpec.Builder();

        COMMON_BUILDER.comment("General settings").push(CATEGORY_GENERAL);
        ALLOW_FLICKER = COMMON_BUILDER.comment("Allow the flicker effect").define("allowFlicker", true);
        COLOR_COUNT = COMMON_BUILDER.comment("Amount of times to try adding an /additional/ color to an explosion").defineInRange("colorCount", 5, 0, 5);

        MIN_HEIGHT = COMMON_BUILDER.comment("The minimum flight time of rockets").defineInRange("minHeight", 3, 1, 10);
        MAX_HEIGHT = COMMON_BUILDER.comment("The maximum /additional/ flight time of rockets (gets added on top of minHeight)").defineInRange("maxHeight", 7, 0, 10);

        COMMON_CONFIG = COMMON_BUILDER.build();
    }

}
