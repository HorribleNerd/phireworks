package com.horriblenerd.phireworks;

import com.google.common.collect.Lists;
import net.minecraft.item.FireworkRocketItem;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.world.IWorld;

import java.util.List;

/**
 * Created by HorribleNerd on 25/12/2020
 */
public class FireworkUtil {

    public static ItemStack getRandomRocket(IWorld world) {
        ItemStack stack = new ItemStack(Items.FIREWORK_ROCKET);

        CompoundNBT compoundnbt = stack.getOrCreateChildTag("Fireworks");
        ListNBT listnbt = new ListNBT();

        for (int i = 0; i < world.getRandom().nextInt(3) + 1; i++) {
            listnbt.add(getRandomExplosion(world, stack));
        }

        compoundnbt.putByte("Flight", (byte) (world.getRandom().nextInt(Config.MAX_HEIGHT.get()) + Config.MIN_HEIGHT.get()));
        compoundnbt.put("Explosions", listnbt);

        return stack;
    }

    public static CompoundNBT getRandomExplosion(IWorld world, ItemStack stack) {
        CompoundNBT compoundnbt = stack.getOrCreateChildTag("Explosion");
//        FireworkRocketItem.Shape fireworkrocketitem$shape = FireworkRocketItem.Shape.get(world.getRandom().nextInt(5)); // Client only for some reason
        FireworkRocketItem.Shape fireworkrocketitem$shape = FireworkRocketItem.Shape.values()[world.getRandom().nextInt(5)];

        if (Config.ALLOW_FLICKER.get()) {
            compoundnbt.putBoolean("Flicker", world.getRandom().nextBoolean());
        }
        compoundnbt.putBoolean("Trail", world.getRandom().nextBoolean());

        List<Integer> colorList = Lists.newArrayList();
        for (int i = 0; i < world.getRandom().nextInt(Config.COLOR_COUNT.get()) + 1; i++) {
            colorList.add((world.getRandom().nextInt()));
        }
        compoundnbt.putIntArray("Colors", colorList);

        List<Integer> fadeList = Lists.newArrayList();
        for (int i = 0; i < world.getRandom().nextInt(3); i++) {
            fadeList.add((world.getRandom().nextInt()));
        }
        compoundnbt.putIntArray("FadeColors", fadeList);

        compoundnbt.putByte("Type", (byte) fireworkrocketitem$shape.getIndex());
        return compoundnbt;
    }

}
