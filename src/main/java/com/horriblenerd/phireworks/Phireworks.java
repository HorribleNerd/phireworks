package com.horriblenerd.phireworks;

import net.minecraft.block.AbstractBlock;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.material.MaterialColor;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.config.ModConfig;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

// The value here should match an entry in the META-INF/mods.toml file
@Mod(Phireworks.MODID)
public class Phireworks {

    public static final String MODID = "phireworks";

    // Directly reference a log4j logger.
    private static final Logger LOGGER = LogManager.getLogger();

    public Phireworks() {
        ModLoadingContext.get().registerConfig(ModConfig.Type.COMMON, Config.COMMON_CONFIG);
        init();
    }

    private static final DeferredRegister<Block> BLOCKS = DeferredRegister.create(ForgeRegistries.BLOCKS, MODID);
    private static final DeferredRegister<Item> ITEMS = DeferredRegister.create(ForgeRegistries.ITEMS, MODID);

    public static void init() {
        BLOCKS.register(FMLJavaModLoadingContext.get().getModEventBus());
        ITEMS.register(FMLJavaModLoadingContext.get().getModEventBus());
    }

    public static final RegistryObject<PhireworkBlock> PHIREWORK_BLOCK = BLOCKS.register("phireworks", () -> new PhireworkBlock(AbstractBlock.Properties.create((new Material.Builder(MaterialColor.RED)).build()).hardnessAndResistance(0.2f)));
    public static final RegistryObject<Item> PHIREWORK_ITEM = ITEMS.register("phireworks", () -> new BlockItem(PHIREWORK_BLOCK.get(), new Item.Properties().group(ItemGroup.REDSTONE)));
}
