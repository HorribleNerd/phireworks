package com.horriblenerd.phireworks;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.dispenser.IBlockSource;
import net.minecraft.dispenser.IDispenseItemBehavior;
import net.minecraft.dispenser.ProxyBlockSource;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.FireworkRocketEntity;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;

/**
 * Created by HorribleNerd on 25/12/2020
 */
public class PhireworkBlock extends Block {
    public static final BooleanProperty POWERED = BlockStateProperties.POWERED;


    public PhireworkBlock(Properties properties) {
        super(properties);
        this.setDefaultState(this.stateContainer.getBaseState().with(POWERED, Boolean.FALSE));
    }

    public void neighborChanged(BlockState state, World worldIn, BlockPos pos, Block blockIn, BlockPos fromPos, boolean isMoving) {
        boolean flag = worldIn.isBlockPowered(pos);
        if (worldIn instanceof ServerWorld) {
            if (flag != state.get(POWERED)) {
                if (flag) {
                    this.dispense((ServerWorld) worldIn, pos);
                }

                worldIn.setBlockState(pos, state.with(POWERED, flag), 3);
            }
        }
    }

    public ActionResultType onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) {
        if (worldIn.isRemote) {
            return ActionResultType.SUCCESS;
        } else {
            if (worldIn instanceof ServerWorld) {
                dispense((ServerWorld) worldIn, pos);
            }
            return ActionResultType.CONSUME;
        }
    }

    protected void dispense(ServerWorld worldIn, BlockPos pos) {
        ProxyBlockSource proxyblocksource = new ProxyBlockSource(worldIn, pos);
        dispenseRocket(proxyblocksource);
    }

    public void dispenseRocket(IBlockSource source) {
        FireworkRocketEntity fireworkrocketentity = new FireworkRocketEntity(source.getWorld(), FireworkUtil.getRandomRocket(source.getWorld()), source.getX(), source.getY(), source.getX(), true);
        Direction direction = Direction.UP;
        IDispenseItemBehavior.dispenseEntity(source, fireworkrocketentity, Direction.UP);
        fireworkrocketentity.shoot((double) direction.getXOffset(), (double) direction.getYOffset(), (double) direction.getZOffset(), (0.25F + source.getWorld().getRandom().nextFloat() * (0.75F - 0.25F)), 1.0F);
        source.getWorld().addEntity(fireworkrocketentity);
    }

    protected void fillStateContainer(StateContainer.Builder<Block, BlockState> builder) {
        builder.add(POWERED);
    }

}
